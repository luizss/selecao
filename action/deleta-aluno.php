<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Alunos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--<link href="https://fonts.googleapis.com/css?family=Raleway:200,400" rel="stylesheet">-->
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Gerenciador</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuario<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="">Sair</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="../forms/cadastro-professor">Cadastrar professor</a></li>
                <li><a href='../forms/cadastro-curso'>Cadastrar cursos</a></li>
                <li><a href='../forms/cadastro-aluno'>Cadastrar aluno</a></li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                        <?php
                        require ("../classes/Aluno.php");
                        if(isset($_SERVER['REQUEST_METHOD']) and $_SERVER['REQUEST_METHOD']=='GET'){
                            if(isset($_GET['a'])){
                                $a = new Aluno();
                                $a->id_aluno = $_GET['a'];
                                $a->deletaAluno();
                            }
                        }
                        ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>
