<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lista de Cursos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--<link href="https://fonts.googleapis.com/css?family=Raleway:200,400" rel="stylesheet">-->
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Painél de controle</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="../forms/cadastro-professor">Cadastrar professor</a></li>
                <?php
                require ("../classes/Curso.php");
                $c = new Curso();
                if($c->getContaprofessor()>0){
                    echo "<li><a href='../forms/cadastro-curso'>Cadastrar cursos</a></li>";
                }
                if($c->getContacurso()>0){
                    echo "<li><a href='../forms/cadastro-aluno'>Cadastrar aluno</a></li>";
                }
                if($c->getContaprofessor()>0 && $c->getContacurso()>0 && $c->getContaaluno()>0){
                    echo "<li><a href='../listapdf'>Relatório em PDF</a></li>";
                }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        $c->listaCurso();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>