<?php
/**
 * Created by PhpStorm.
 * User: Luiz Souza
 * Date: 08/07/2018
 * Time: 22:16
 */
require ("Escola.php");
class Aluno extends Escola{
    public $id_aluno;
    public $nome;
    public $data_nascimento;
    public $logradouro;
    public $numero;
    public $bairro;
    public $cidade;
    public $estado;
    public $data_criacao;
    public $cep;
    public $id_curso;
    public $nome_curso;
    private $qtd_alunos;

    function cadastraAluno(){
        $sql = Connection::getConn()->prepare("insert into aluno set nome_aluno=?,data_nascimento=?,logradouro=?,numero=?,bairro=?,cidade=?,estado=?,cep=?,id_curso=?");
        if($sql->execute(array($this->nome,$this->data_nascimento,$this->logradouro,$this->numero,$this->bairro,$this->cidade,$this->estado,$this->cep,$this->id_curso))){
            echo "<p class='bg-success'>Cadastro realizado</p>";
        }
    }

    function editarAluno(){
        $sql = Connection::getConn()->prepare("update aluno set nome_aluno=?,data_nascimento=?,logradouro=?,numero=?,bairro=?,cidade=?,estado=?,cep=?,id_curso=? WHERE id_aluno=?");
        if($sql->execute(array($this->nome,$this->data_nascimento,$this->logradouro,$this->numero,$this->bairro,$this->cidade,$this->estado,$this->cep,$this->id_curso,$this->id_aluno))){
            echo "<p class='bg-success'>Edição realizada</p>";
        }
    }

    function deletaAluno(){
        $sql = Connection::getConn()->prepare("delete from aluno where id_aluno=?");
        if($sql->execute(array($this->id_aluno))){
            echo "<p class='bg-success'>Regitro excluído</p>";
            echo "<a class='btn btn-primary' href='lista-aluno'>Lista de alunos</a>";
        }
    }
    function getBuscaDadosAluno(){
        $sql = Connection::getConn()->prepare("select * from aluno where nome_aluno=? and id_curso=?");
        if($sql->execute(array($this->nome,$this->id_curso))){
            $this->qtd_alunos = $sql->rowCount();
            return $this->qtd_alunos;
        }
    }
    function registroAlunoExistente(){
        echo "<p class='bg-warning'>Registro já existente</p>";
    }
    function msgErroNome(){
        echo "<p class='bg-warning'>Informe o seu nome</p>";
    }
    function msgErroDataNasc(){
        echo "<p class='bg-warning'>Informe e data de nascimento</p>";
    }
    function msgErroLogradouro(){
        echo "<p class='bg-warning'>Informe o logradouro</p>";
    }
    function msgErroNumero(){
        echo "<p class='bg-warning'>Informe o número da sua casa</p>";
    }
    function msgErroBairro(){
        echo "<p class='bg-warning'>Informe seu bairro</p>";
    }
    function msgErroCidade(){
        echo "<p class='bg-warning'>Informe a cidade</p>";
    }
    function msgErroEstado(){
        echo "<p class='bg-warning'>Informe o estado</p>";
    }
    function msgErroCep(){
        echo "<p class='bg-warning'>Informe e CEP</p>";
    }
    function msgErroCurso(){
        echo "<p class='bg-warning'>Informe o curso.</p>";
    }

    function listaAluno(){
        echo "<div class='row'>
                <div class='col-lg-12'>
                    <h1 class='page-header'>
                        Lista - <small>Alunos</small>
                    </h1>
                </div>
            </div>";
        $sql = Connection::getConn()->prepare("select * from aluno");
        if($sql->execute()){
            if($sql->rowCount()>0){
                echo "<div class='panel-body'><div class='list-group'>";
                foreach($sql as $value){
                    $this->id_aluno = $value["id_aluno"];
                    $this->nome = $value["nome_aluno"];
                    echo "<div class='list-group-item'>$this->nome
                            <span class='pull-right'>
                            <a href='../forms/edita-aluno?a=$this->id_aluno' class=''>Editar</a> &nbsp; <a href='deleta-aluno?a=$this->id_aluno' class=''>deletar</a>
                            </span>
                          </div>";
                }
                echo "</div></div>";
            }else{
                echo "<h3 class='bg-warning'>Não há alunos cadastrados no momento</h3>";
            }
        }
    }


}