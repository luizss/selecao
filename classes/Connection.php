<?php
/**
 * Created by PhpStorm.
 * User: Luiz Souza
 * Date: 08/07/2018
 * Time: 22:35
 */

class Connection {
    private static $conn;
    static function getConn(){
        if(is_null(self::$conn)){
            self::$conn = new PDO("mysql:host=localhost;dbname=escola","root","");
            /*self::$conn = new PDO("mysql:host=mysql.hostinger.com.br;dbname=u934980854_lib","u934980854_esc","smellslike");*/
        }
        return self::$conn;
    }
}