<?php
/**
 * Created by PhpStorm.
 * User: Luiz Souza
 * Date: 08/07/2018
 * Time: 22:16
 */
require("Escola.php");
class Curso extends Escola{
    public $id_curso;
    public $nome;
    public $data_criacao;
    public $id_professor;
    public $nome_professor;
    private $qtd_cursos;
    function cadastraCurso(){
        $sql = Connection::getConn()->prepare("insert into curso set nome_curso=?,id_professor=?");
        if($sql->execute(array($this->nome,$this->id_professor))){
            echo "<p class='bg-success'>Cadastro realizado</p>";
        }
    }
    function editarCurso(){
        $sql = Connection::getConn()->prepare("update curso set nome_curso=?,id_professor=? where id_curso=?");
        if($sql->execute(array($this->nome,$this->id_professor,$this->id_curso))){
            echo "<p class='bg-success'>Edição realizada</p>";
        }
    }
    function deletaCurso(){
        $sql = Connection::getConn()->prepare("delete from aluno where id_curso=?");
        if($sql->execute(array($this->id_curso))){
            $sql = Connection::getConn()->prepare("delete from curso where id_curso=?");
            if($sql->execute(array($this->id_curso))){
                echo "<p class='bg-success'>Regitro excluído</p>";
                echo "<a class='btn btn-primary' href='lista-curso'>Lista de cursos</a>";
            }
        }
    }
    function listaCurso(){
        echo "<div class='row'>
                <div class='col-lg-12'>
                    <h1 class='page-header'>
                        Lista - <small>Cursos</small>
                    </h1>
                </div>
            </div>";
        $sql = Connection::getConn()->prepare("select * from curso");
        if($sql->execute()){
            if($sql->rowCount()>0){
                echo "<div class='panel-body'><div class='list-group'>";
                foreach($sql as $value){
                    $this->id_curso = $value["id_curso"];
                    $this->nome = $value["nome_curso"];
                    echo "<div class='list-group-item'>$this->nome
                            <span class='pull-right'>
                            <a href='../forms/edita-curso?c=$this->id_curso'>Editar</a> &nbsp; <a href='deleta-curso?c=$this->id_curso'>deletar</a>
                            </span>
                          </div>";
                }
                echo "</div></div>";
            }else{
                echo "<h3 class='bg-warning'>Não há cursos cadastrados no momento</h3>";
            }
        }
    }

    function getBuscaDadosCurso(){
        $sql = Connection::getConn()->prepare("select * from curso where nome_curso=? and id_professor=?");
        if($sql->execute(array($this->nome,$this->id_professor))){
            $this->qtd_cursos = $sql->rowCount();
            return $this->qtd_cursos;
        }
    }
    function registroCursoExistente(){
        echo "<p class='bg-warning'>Registro já existente</p>";
    }
    function erroCampoNome(){
        echo "<p class='bg-warning'>Informe corretamente o nome</p>";
    }
    function erroCampoProfessor(){
        echo "<p class='bg-warning'>Escolha o professor</p>";
    }
}