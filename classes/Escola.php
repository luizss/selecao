<?php
/**
 * Created by PhpStorm.
 * User: Luiz Souza
 * Date: 06/07/2018
 * Time: 12:17
 */
require ("Connection.php");
class Escola {
    private $id_professor;
    private $id_curso;
    private $id_aluno;
    public $professor;
    public $cursos;
    private $alunos;
    function getContaprofessor(){
        $sql = Connection::getConn()->prepare("select * from professor");
        if($sql->execute()){
            $this->professor = $sql->rowCount();
            return $this->professor;
        }
    }
    function getContacurso(){
        $sql = Connection::getConn()->prepare("select * from curso");
        if($sql->execute()){
            $this->cursos = $sql->rowCount();
            return $this->cursos;
        }
    }
    function getContaaluno(){
        $sql = Connection::getConn()->prepare("select * from aluno");
        if($sql->execute()){
            $this->alunos = $sql->rowCount();
            return $this->alunos;
        }
    }
    function selectProfessor(){
        $sql = Connection::getConn()->prepare("select * from professor");
        if($sql->execute()){
            foreach($sql as $value){
                $this->id_professor = $value["id_professor"];
                $this->professor = $value["nome_professor"];
                echo "<option value='$this->id_professor'>$this->professor</option>";
            }
        }
    }
    function selectCurso(){
        $sql = Connection::getConn()->prepare("select * from curso");
        if($sql->execute()){
            foreach($sql as $value){
                $this->id_curso = $value["id_curso"];
                $this->cursos = $value["nome_curso"];
                echo "<option value='$this->id_curso'>$this->cursos</option>";
            }
        }
    }
}