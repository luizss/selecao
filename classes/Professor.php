<?php
/**
 * Created by PhpStorm.
 * User: Luiz Souza
 * Date: 08/07/2018
 * Time: 22:15
 */
require ("Escola.php");
class Professor extends Escola{
    public $id_professor;
    public $nome;
    public $data_nascimento;
    public $data_criacao;
    private $qtd_professores;
    function cadastraProfessor(){
        $sql = Connection::getConn()->prepare("insert into professor set nome_professor=?,data_nascimento=?");
        if($sql->execute(array($this->nome,$this->data_nascimento))){
            echo "<p class='bg-success'>Cadastro realizado</p>";
        }
    }
    function editarProfessor(){
        $sql = Connection::getConn()->prepare("update professor set nome_professor=?,data_nascimento=? where id_professor=?");
        if($sql->execute(array($this->nome,$this->data_nascimento,$this->id_professor))){
            echo "<p class='bg-success'>Edição realizada</p>";
        }
    }
    function deletaProfessor(){
        $sql = Connection::getConn()->prepare("select * from curso where id_professor=?");
        if($sql->execute(array($this->id_professor))){
            if($sql->rowCount()>0){
                foreach($sql as $value){
                    $this->cursos = $value["id_curso"];
                    $sql = Connection::getConn()->prepare("delete from aluno where id_curso=?");
                    if($sql->execute(array($this->cursos))){
                        $sql = Connection::getConn()->prepare("delete from curso where id_professor=?");
                        if($sql->execute(array($this->id_professor))){
                            $sql = Connection::getConn()->prepare("delete from professor where id_professor=?");
                            if($sql->execute(array($this->id_professor))){
                                echo "<p class='bg-success'>Regitro excluído</p>";
                                echo "<a class='btn btn-primary' href='lista-professor'>Lista de Professores</a>";
                            }
                        }
                    }
                }
            }else{
                $sql = Connection::getConn()->prepare("delete from professor where id_professor=?");
                if($sql->execute(array($this->id_professor))){
                    echo "<p class='bg-success'>Regitro excluído</p>";
                    echo "<a class='btn btn-primary' href='lista-professor'>Lista de Professores</a>";
            }
        }
     }
    }
    function listaProfessor(){
        echo "<div class='row'>
                <div class='col-lg-12'>
                    <h1 class='page-header'>
                        Lista - <small>Professores</small>
                    </h1>
                </div>
            </div>";
        $sql = Connection::getConn()->prepare("select * from professor");
        if($sql->execute()){
            if($sql->rowCount()>0){
                echo "<div class='panel-body'><div class='list-group'>";
                foreach($sql as $value){
                    $this->id_professor = $value["id_professor"];
                    $this->nome = $value["nome_professor"];
                    echo "<div class='list-group-item'>$this->nome
                           <span class='pull-right'>
                           <a href='../forms/edita-professor?p=$this->id_professor' class=''>Editar</a> &nbsp; <a href='deleta-professor?p=$this->id_professor' class=''>deletar</a>
                           </span>
                          </div>";
                }
                echo "</div></div>";
            }else{
                echo "<h3 class='bg-warning'>Não há professores cadastrados no momento</h3>";
            }
        }
    }

    function erroCampoNome(){
        echo "<p class='bg-warning'>Informe o nome</p>";
    }
    function erroCampoDataNascimento(){
        echo "<p class='bg-warning'>Informe a data</p>";
    }
    function getBuscaDados(){
        $sql = Connection::getConn()->prepare("select * from professor where nome_professor=? and data_nascimento=?");
        if($sql->execute(array($this->nome,$this->data_nascimento))){
            $this->qtd_professores = $sql->rowCount();
            return $this->qtd_professores;
        }
    }
    function registroExistente(){
        echo "<p class='bg-warning'>Professor já cadastrado!</p>";
    }

}