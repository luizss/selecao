<?php
require ("../classes/Aluno.php");
$a = new Aluno();
if($a->getContacurso()==0){
    header("location:../index");
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Novo aluno</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/cep.js"></script>
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Painél de controle</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="cadastro-professor">Cadastrar professor</a></li>
                <li><a href='cadastro-curso'>Cadastrar cursos</a></li>
                <li><a href='cadastro-aluno'>Cadastrar aluno</a></li>
                <?php
                if($a->getContaprofessor()>0 && $a->getContacurso()>0 && $a->getContaaluno()>0){
                    echo "<li><a href='../listapdf'>Relatório em PDF</a></li>";
                }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Cadastro - <small>Aluno</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if(isset($_SERVER['REQUEST_METHOD']) and $_SERVER['REQUEST_METHOD']=='POST'){
                      $a->nome = $_POST["nome"];
                      $a->data_nascimento = $_POST["data_nascimento"];
                      $a->cep = $_POST["cep"];
                      $a->estado = $_POST["estado"];
                      $a->logradouro = $_POST["logradouro"];
                      $a->numero = $_POST["numero"];
                      $a->bairro = $_POST["bairro"];
                      $a->cidade = $_POST["cidade"];
                      $a->id_curso = $_POST["curso"];
                      if(strlen($a->nome)<10 || strlen($a->nome)>100){
                          $a->msgErroNome();
                      }else if($a->data_nascimento==null){
                          $a->msgErroDataNasc();
                      }else if($a->cep==null){
                          $a->msgErroCep();
                      }else if($a->estado==null){
                          $a->msgErroEstado();
                      }else if(strlen($a->logradouro)<4 || strlen($a->logradouro)>50){
                          $a->msgErroLogradouro();
                      }else if($a->numero==null){
                          $a->msgErroNumero();
                      }else if($a->bairro==null){
                          $a->msgErroBairro();
                      }else if($a->cidade==null){
                          $a->msgErroCidade();
                      }else if($a->id_curso==0){
                          $a->msgErroCurso();
                      }else if($a->getBuscaDadosAluno()>0){
                          $a->registroAlunoExistente();
                      }else{
                          $a->cadastraAluno();
                      }
                    }
                    ?>
                    <form method="post" action="?">

                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Digite seu nome">
                        </div>
                        <div class="form-group">
                            <label for="data_nascimento">Data de nascimento</label>
                            <input type="date" id="data_nascimento" name="data_nascimento" class="form-control">
                        </div>

                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="cep">CEP</label>
                                <input type="text" id="cep" name="cep" class="form-control" placeholder="Informe o CEP">
                            </div>
                        
                            <div class="col-md-6">
                                <label for="estado">Estado</label>
                                <input type="text" id="uf" name="estado" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-9">
                                <label for="logradouro">Logradouro</label>
                                <input type="text" id="rua" name="logradouro" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="numero">Número</label>
                                <input type="text" id="numero" name="numero" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="bairro">Bairro</label>
                                <input type="text" id="bairro" name="bairro" class="form-control" >
                            </div>
                            <div class="col-md-6">
                                <label for="cidade">Cidade</label>
                                <input type="text" id="cidade" name="cidade" class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <select name="curso" class="form-control">
                                <option value="0">SELECIONE O CURSO...</option>
                                <?php
                                    $a->selectCurso();
                                ?>
                            </select>
                        </div>
                        <div class="btn btn-primary">
                            <i class="fas fa-save"></i>
                            <input type="submit" value="Cadastrar">
                        </div>
                        <div class="btn btn-primary">
                            <i class="fas fa-angle-double-left pull-left"></i>
                            <input type="button" value="Voltar ao painél" onclick="window.open('../index','_self');">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>