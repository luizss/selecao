<?php
    require ("../classes/Curso.php");
    $c = new Curso();
    if($c->getContaprofessor()==0){
        header("location:../");
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Novo curso</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Painél de controle</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="cadastro-professor">Cadastrar professor</a></li>
                <li><a href='cadastro-curso'>Cadastrar cursos</a></li>
                <?php
                if($c->getContacurso()>0){
                    echo "<li><a href='cadastro-aluno'>Cadastrar aluno</a></li>";
                }
                if($c->getContaprofessor()>0 && $c->getContacurso()>0 && $c->getContaaluno()>0){
                    echo "<li><a href='../listapdf'>Relatório em PDF</a></li>";
                }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Cadastro - <small>Curso</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='POST'){
                            $c->nome = $_POST["nome"];
                            $c->id_professor = $_POST["professor"];
                            if(strlen($c->nome)<5 || strlen($c->nome)>100){
                                $c->erroCampoNome();
                            }else if($c->id_professor==0){
                                $c->erroCampoProfessor();
                            }else if($c->getBuscaDadosCurso()>0){
                                $c->registroCursoExistente();
                            }else{
                                $c->cadastraCurso();
                            }
                        }
                    ?>
                    <form method="post" action="?">
                        <div class="form-group">
                            <label for="nome">Nome do curso</label>
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Digite o nome do curso">
                        </div>
                        <div class="form-group">
                            <label for="data_nascimento">Professor</label>
                            <select name="professor" class="form-control">
                                <option value="0">Escolha o professor...</option>
                                <?php $c->selectProfessor();?>
                            </select>
                        </div>
                        <div class="btn btn-primary">
                            <i class="fas fa-save"></i>
                            <input type="submit" value="Cadastrar">
                        </div>
                        <div class="btn btn-primary">
                            <i class="fas fa-angle-double-left pull-left"></i>
                            <input type="button" value="Voltar ao painél" onclick="window.open('../index','_self');">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>