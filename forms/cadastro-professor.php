<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Novo professor</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Painél de controle</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="cadastro-professor">Cadastrar professor</a></li>
                <?php
                require ("../classes/Professor.php");
                $p = new Professor();
                if($p->getContaprofessor()>0){
                    echo "<li><a href='cadastro-curso'>Cadastrar cursos</a></li>";
                }
                if($p->getContacurso()>0){
                    echo "<li><a href='cadastro-aluno'>Cadastrar aluno</a></li>";
                }
                if($p->getContaprofessor()>0 && $p->getContacurso()>0 && $p->getContaaluno()>0){
                    echo "<li><a href='../listapdf'>Relatório em PDF</a></li>";
                }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Cadastro - <small>Professor</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                        if(isset($_SERVER['REQUEST_METHOD']) and $_SERVER['REQUEST_METHOD']=='POST'){
                            $p->nome = $_POST['nome'];
                            $p->data_nascimento = $_POST['data_nascimento'];
                            if(strlen($p->nome)<10 || strlen($p->nome>100)){
                               $p->erroCampoNome();
                            }else if($p->data_nascimento==null){
                                $p->erroCampoDataNascimento();
                            }else if($p->getBuscaDados()>0){
                                $p->registroExistente();
                            }else{
                                $p->cadastraProfessor();
                            }
                            }
                    ?>
                    <form method="post" action="?">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Digite seu nome">
                        </div>
                        <div class="form-group">
                            <label for="data_nascimento">Data de nascimento</label>
                            <input type="date" id="data_nascimento" name="data_nascimento" class="form-control">
                        </div>
                        <div class="btn btn-primary">
                            <i class="fas fa-save"></i>
                            <input type="submit" value="Cadastrar">
                        </div>
                        <div class="btn btn-primary">
                            <i class="fas fa-angle-double-left pull-left"></i>
                            <input type="button" value="Voltar ao painél" onclick="window.open('../index','_self');">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>