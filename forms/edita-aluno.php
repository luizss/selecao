<?php
require ("../classes/Aluno.php");
$a = new Aluno();
if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='GET'){
    if(isset($_GET['a'])){
        $a->id_aluno = $_GET['a'];
        $sql = Connection::getConn()->prepare("select aluno.nome_aluno,aluno.data_nascimento,aluno.logradouro,aluno.numero,aluno.bairro,aluno.cidade,aluno.estado,aluno.cep,aluno.id_curso,curso.nome_curso from aluno inner join curso on aluno.id_curso=curso.id_curso where id_aluno=?");
        if($sql->execute(array($a->id_aluno))){
            foreach($sql as $value){
                $a->nome = $value["nome_aluno"];
                $a->data_nascimento = $value["data_nascimento"];
                $a->logradouro = $value["logradouro"];
                $a->numero = $value["numero"];
                $a->bairro = $value["bairro"];
                $a->cidade = $value["cidade"];
                $a->estado = $value["estado"];
                $a->cep = $value["cep"];
                $a->id_curso = $value["id_curso"];
                $a->nome_curso = $value["nome_curso"];
            }
        }
    }else{
        header("location:../");
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Novo professor</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/cep.js"></script>
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Gerenciador</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuario<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="">Sair</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="cadastro-professor">Cadastrar professor</a></li>
                <li><a href="cadastro-curso">Cadastrar cursos</a></li>
                <?php
                if($a->getContacurso()>0){
                    echo "<li><a href='cadastro-aluno'>Cadastrar aluno</a></li>";
                }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Edição - <small>Professor</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='POST') {
                        $a->nome = $_POST["nome"];
                        $a->data_nascimento = $_POST["data_nascimento"];
                        $a->cep = $_POST["cep"];
                        $a->estado = $_POST["estado"];
                        $a->logradouro = $_POST["logradouro"];
                        $a->numero = $_POST["numero"];
                        $a->bairro = $_POST["bairro"];
                        $a->cidade = $_POST["cidade"];
                        $a->id_curso = $_POST["curso"];
                        $a->id_aluno = $_POST["id_aluno"];
                        if(strlen($a->nome)<10 || strlen($a->nome)>100){
                            $a->msgErroNome();
                        }else if($a->data_nascimento==null){
                            $a->msgErroDataNasc();
                        }else if($a->cep==null){
                            $a->msgErroCep();
                        }else if($a->estado==null){
                            $a->msgErroEstado();
                        }else if(strlen($a->logradouro)<4 || strlen($a->logradouro)>50){
                            $a->msgErroLogradouro();
                        }else if($a->numero==null){
                            $a->msgErroNumero();
                        }else if($a->bairro==null){
                            $a->msgErroBairro();
                        }else if($a->cidade==null){
                            $a->msgErroCidade();
                        }else if($a->id_curso==0){
                            $a->msgErroCurso();
                        }else{
                            $a->editarAluno();
                        }
                    }
                    ?>
                    <form method="post" action="?">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Digite seu nome" value="<?php echo $a->nome;?>">
                        </div>
                        <div class="form-group">
                            <label for="data_nascimento">Data de nascimento</label>
                            <input type="date" id="data_nascimento" name="data_nascimento" class="form-control" value="<?php echo $a->data_nascimento;?>">
                        </div>

                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="cep">CEP</label>
                                <input type="text" id="cep" name="cep" class="form-control" value="<?php echo $a->cep;?>">
                            </div>

                            <div class="col-md-6">
                                <label for="estado">Estado</label>
                                <input type="text" id="uf" name="estado" class="form-control" value="<?php echo $a->estado;?>">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-9">
                                <label for="logradouro">Logradouro</label>
                                <input type="text" id="rua" name="logradouro" class="form-control" value="<?php echo $a->logradouro;?>">
                            </div>
                            <div class="col-md-3">
                                <label for="numero">Número</label>
                                <input type="text" id="numero" name="numero" class="form-control" value="<?php echo $a->numero;?>">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="bairro">Bairro</label>
                                <input type="text" id="bairro" name="bairro" class="form-control" value="<?php echo $a->bairro;?>">
                            </div>
                            <div class="col-md-6">
                                <label for="cidade">Cidade</label>
                                <input type="text" id="cidade" name="cidade" class="form-control" value="<?php echo $a->cidade;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <select name="curso" class="form-control">
                                <option value="<?php echo $a->id_curso;?>"><?php echo $a->nome_curso;?></option>
                                <?php
                                $a->selectCurso();
                                ?>
                            </select>
                        </div>

                        <input type="hidden" name="id_aluno" value="<?php echo $a->id_aluno;?>">

                        <input type="submit" class="btn btn-primary" value="Editar">
                        <a class="btn btn-primary" href="../">Voltar ao painél</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>