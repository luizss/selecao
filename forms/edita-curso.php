<?php
require ("../classes/Curso.php");
$c = new Curso();
if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='GET'){
    if(isset($_GET['c'])){
        $c->id_curso = $_GET['c'];
        $sql = Connection::getConn()->prepare("select curso.nome_curso,curso.id_professor,professor.nome_professor from curso inner join professor on curso.id_professor=professor.id_professor where id_curso=?");
        if($sql->execute(array($c->id_curso))){
            foreach($sql as $value){
                $c->nome = $value["nome_curso"];
                $c->id_professor = $value["id_professor"];
                $c->nome_professor = $value["nome_professor"];
            }
        }
    }else{
        header("location:../");
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Novo professor</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--<link href="https://fonts.googleapis.com/css?family=Raleway:200,400" rel="stylesheet">-->
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Gerenciador</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuario<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="">Sair</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="cadastro-professor">Cadastrar professor</a></li>
                <li><a href="cadastro-curso">Cadastrar cursos</a></li>
                <?php
                if($c->getContacurso()>0){
                    echo "<li><a href='cadastro-aluno'>Cadastrar aluno</a></li>";
                }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Edição - <small>Professor</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='POST'){
                            $c->nome = $_POST["nome"];
                            $c->id_professor = $_POST["professor"];
                            $c->id_curso = $_POST["id_curso"];
                            if(strlen($c->nome)<5 || strlen($c->nome)>100){
                                $c->erroCampoNome();
                            }else if($c->id_professor==0){
                                $c->erroCampoProfessor();
                            }else{
                                $c->editarCurso();
                            }
                        }
                    ?>
                    <form method="post" action="?">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Digite seu nome" value="<?php echo $c->nome;?>">
                        </div>
                        <div class="form-group">
                            <label for="data_nascimento">Professor</label>
                            <select name="professor" class="form-control">
                                <option value="<?php echo $c->id_professor;?>"><?php echo $c->nome_professor;?></option>
                                <?php $c->selectProfessor();?>
                            </select>
                        </div>
                        <input type="hidden" name="id_curso" value="<?php echo $c->id_curso;?>">

                        <input type="submit" class="btn btn-primary" value="Editar">
                        <a class="btn btn-primary" href="../">Voltar ao painél</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>