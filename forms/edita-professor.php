<?php
require ("../classes/Professor.php");
$p = new Professor();
    if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='GET'){
        if(isset($_GET['p'])){
            $p->id_professor = $_GET['p'];
            $sql = Connection::getConn()->prepare("select * from professor where id_professor=?");
            if($sql->execute(array($p->id_professor))){
                foreach($sql as $value){
                    $p->nome = $value["nome_professor"];
                    $p->data_nascimento = $value["data_nascimento"];
                }
            }
        }else{
            header("location:../");
        }
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Novo professor</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="../css/morris.css">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!--<link href="https://fonts.googleapis.com/css?family=Raleway:200,400" rel="stylesheet">-->
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../" class="navbar-brand" title="painel">Gerenciador</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuario<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="">Sair</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="cadastro-professor">Cadastrar professor</a></li>
                <?php
                if($p->getContaprofessor()>0){
                    echo "<li><a href='cadastro-curso'>Cadastrar cursos</a></li>";
                }
                if($p->getContacurso()>0){
                    echo "<li><a href='cadastro-aluno'>Cadastrar aluno</a></li>";
                }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Edição - <small>Professor</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if(isset($_SERVER['REQUEST_METHOD']) and $_SERVER['REQUEST_METHOD']=='POST'){
                        $p->nome = $_POST['nome'];
                        $p->data_nascimento = $_POST['data_nascimento'];
                        $p->id_professor = $_POST["id_professor"];
                        if(strlen($p->nome)<10 || strlen($p->nome>100)){
                            $p->erroCampoNome();
                        }else if($p->data_nascimento==null){
                            $p->erroCampoDataNascimento();
                        }else{
                            $p->editarProfessor();
                        }
                    }
                    ?>
                    <form method="post" action="?">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Digite seu nome" value="<?php echo $p->nome;?>">
                        </div>
                        <div class="form-group">
                            <label for="data_nascimento">Data de nascimento</label>
                            <input type="date" id="data_nascimento" name="data_nascimento" class="form-control" value="<?php echo $p->data_nascimento;?>">
                        </div>
                        <input type="hidden" name="id_professor" value="<?php echo $p->id_professor;?>">
                        <input type="submit" class="btn btn-primary" value="Editar">
                        <a class="btn btn-primary" href="../">Voltar ao painél</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/raphael.min.js"></script>
<script src="../js/morris.min.js"></script>
<script src="../js/morris-data.js"></script>
</body>
</html>