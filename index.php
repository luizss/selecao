<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrador</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="css/morris.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index" class="navbar-brand" title="painel">Painél de controle</a>
            <!--<a class="navbar-brand" href="#">Painél de controle</a>-->
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li><a href="forms/cadastro-professor">Cadastrar professor</a></li>
                <?php
                    require ("classes/Escola.php");
                    $e = new Escola();
                    if($e->getContaprofessor()>0){
                        echo "<li><a href='forms/cadastro-curso.php'>Cadastrar cursos</a></li>";
                    }
                    if($e->getContacurso()>0){
                        echo "<li><a href='forms/cadastro-aluno.php'>Cadastrar aluno</a></li>";
                    }
                    if($e->getContaprofessor()>0 && $e->getContacurso()>0 && $e->getContaaluno()>0){
                        echo "<li><a href='listapdf'>Relatório em PDF</a></li>";
                    }
                ?>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Administrador - <small>Dados</small>
                    </h1>
                </div>
            </div>
            <!-- /.row -->
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12 text-right">
                                    <div class="huge"><?php echo $e->getContaprofessor();?></div>
                                    <div>Professor(es) cadastrado(s)</div>
                                </div>
                            </div>
                        </div>
                        <a href="action/lista-professor">
                            <div class="panel-footer">
                                <i class="fas fa-angle-double-right pull-right"></i>
                                <span class="pull-left">Ver lista</span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12 text-right">
                                    <div class="huge"><?php echo $e->getContacurso();?></div>
                                    <div>Curso(s) cadastrado(s)</div>
                                </div>
                            </div>
                        </div>
                        <a href="action/lista-curso">
                            <div class="panel-footer">
                                <i class="fas fa-angle-double-right pull-right"></i>
                                <span class="pull-left">Ver lista</span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12 text-right">
                                    <div class="huge"><?php echo $e->getContaaluno();?></div>
                                    <div>Aluno(s) cadastrado(s)</div>
                                </div>
                            </div>
                        </div>
                        <a href="action/lista-aluno">
                            <div class="panel-footer">
                                <i class="fas fa-angle-double-right pull-right"></i>
                                <span class="pull-left">Ver lista</span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
            <!-- /.row -->

            <!-- /.row -->

            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/raphael.min.js"></script>
<script src="js/morris.min.js"></script>
<script src="js/morris-data.js"></script>
</body>
</html>