<?php

require ("classes/Aluno.php");
$a = new Aluno();
define('FPDF_FONTPATH','font/');
require('pdf/fpdf.php');
$pdf = new FPDF('P','cm','A4');

$pdf->AddPage();
$pdf->SetFont('Arial','B','12');

$sql = Connection::getConn()->prepare("select aluno.nome_aluno,curso.nome_curso,professor.nome_professor from aluno inner join curso on aluno.id_curso=curso.id_curso inner join professor on curso.id_professor=professor.id_professor");

if($sql->execute()){
    $pdf->Cell(19.5,1,utf8_decode("Relatório"),1,1,'C');
    $pdf->Cell(7.5,1,utf8_decode("Aluno"),1,0,'C');
    $pdf->Cell(6,1,utf8_decode("Curso"),1,0,'C');
    $pdf->Cell(6,1,utf8_decode("Professor"),1,1,'C');
    foreach($sql as $value){
        $a->nome = $value["nome_aluno"];
        $a->nome_curso = $value["nome_curso"];
        $a->professor = $value["nome_professor"];
        $pdf->Cell(7.5,1,utf8_decode($a->nome),1,0,'C');
        $pdf->Cell(6,1,utf8_decode($a->nome_curso),1,0,'C');
        $pdf->Cell(6,1,utf8_decode($a->professor),1,1,'C');
    }
    $pdf->Output();
}
?>